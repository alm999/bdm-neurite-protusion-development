# Additional resources
Paraview export tutorial: https://www.youtube.com/watch?v=7Ibtgs_wTX4 \
Cell division: https://www.youtube.com/watch?v=rdfz5Ji684c \
Diffusion: https://www.youtube.com/watch?v=y79tjmBFEz8 \
Neuron development: https://www.youtube.com/watch?v=ukkQ-rAX6Fo \
Neuron growth with Spines and Boutons: https://www.youtube.com/watch?v=mAE8bpWCRvw

# cells
BioDynaMo project where cells grow and have a probability to divide after set threshold. The probability increases and it's reported in the standard output.

# diffusion
BioDynaMo demo where cells follow the gradient towards a secreted substance. A module was configured to collect data.

# video
Project for the grounds of the development of synapses for BioDynaMo.

# Acknowledgements
Thanks to Roman Bauer for the opportunity, to Lukas Breitwieser for his help and valuable code aids, and to all the BioDynaMo development team and user base for the continuous support and community 
