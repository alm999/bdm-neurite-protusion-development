// -----------------------------------------------------------------------------
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef COMMON_H_
#define COMMON_H_

#include "biodynamo.h"
#include "core/substance_initializers.h"
#include "my-neurite.h"
#include "my-soma.h"
#include "synapse.h"
#include "biology-modules.h"
#include "neuroscience/neuroscience.h"

namespace bdm {
namespace experimental {
namespace neuroscience {

struct NeuronBuilder {
  ResourceManager* rm_;

  NeuronBuilder() { rm_ = Simulation::GetActive()->GetResourceManager(); }

  void operator()(const Double3& position) {
    auto* soma = new MySoma(position);
    soma->SetDiameter(10);
    auto soma_soptr = soma->GetSoPtr<MySoma>();
    rm_->push_back(soma);

    MyNeurite neurite; // neurite prototype
    auto* axon = bdm_static_cast<MyNeurite*>(
        soma->ExtendNewNeurite({0, 0, 1}, &neurite));
    axon->SetAxon(true);
    axon->AddBiologyModule(new ApicalElongation_BM());
    // axon->AddBiologyModule(new RandomizeColour());
    axon->AddBiologyModule(new ProtusionGrowth());
    axon->SetCanBranch(true);

    auto* dendrite = bdm_static_cast<MyNeurite*>(
        soma->ExtendNewNeurite({0, 0, -1}, &neurite));
    dendrite->AddBiologyModule(new BasalElongation_BM());
    dendrite->AddBiologyModule(new ProtusionGrowth());
    dendrite->SetCanBranch(true);
  }
};


/// define substance for neurite attraction
inline void DefineSubstances(const Param* param) {
  ModelInitializer::DefineSubstance(kSubstanceApical, "substance_apical", 0, 0,
                                    450 / 80);
  ModelInitializer::DefineSubstance(kSubstanceBasal, "substance_basal", 0, 0,
                                    450 / 80);
  // create substance with gaussian distribution for neurite attraction
  ModelInitializer::InitializeSubstance(
      kSubstanceApical, "substance_apical",
      GaussianBand(450, 200, Axis::kZAxis));
  ModelInitializer::InitializeSubstance(
      kSubstanceBasal, "substance_basal",
      GaussianBand(0, 200, Axis::kZAxis));
}


}  // namespace neuroscience
}  // namespace experimental
}  // namespace bdm

#endif  // COMMON_H_
