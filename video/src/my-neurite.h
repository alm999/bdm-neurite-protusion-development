// -----------------------------------------------------------------------------
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef MY_NEURITE_H_
#define MY_NEURITE_H_

#include<cmath>

#include "core/sim_object/sim_object.h"
#include "my-soma.h"
#include "neuroscience/neurite_element.h"

namespace bdm {
namespace experimental{
namespace neuroscience{

// Define my custom neurite MyNeurite, which extends NeuriteElement
class MyNeurite : public experimental::neuroscience::NeuriteElement {
  BDM_SIM_OBJECT_HEADER(MyNeurite, experimental::neuroscience::NeuriteElement,
                        1, can_branch_, its_soma_, colour_);

 public:
  MyNeurite() : Base() {}

  virtual ~MyNeurite() {}

  /// Default event constructor
  MyNeurite(const Event& event, SimObject* other, uint64_t new_oid = 0)
      : Base(event, other, new_oid) {
    if (event.GetId() ==
        experimental::neuroscience::NewNeuriteExtensionEvent::kEventId) {
      its_soma_ = static_cast<MySoma*>(other)->GetSoPtr<MySoma>();
    } else {
      its_soma_ = static_cast<MyNeurite*>(other)->its_soma_;
      if(bdm_static_cast<MyNeurite*>(other)->IsAxon()){
          SetAxon(true);
      }
    }
  }

  /// Default event handler
  void EventHandler(const Event& event, SimObject* other1,
                    SimObject* other2 = nullptr) override{
    Base::EventHandler(event, other1, other2);
  }

  void SetCanBranch(int b) { can_branch_ = b; }
  bool GetCanBranch() const { return can_branch_; }

  void SetMySoma(SoPointer<MySoma> soma) { its_soma_ = soma; }
  SoPointer<MySoma> GetMySoma() const { return its_soma_; }

  void SetColour(double colour){ colour_ = colour; }
  double GetColour(){ return colour_; }

  void AddProtusion(){ protusions_ += 1; }
  void SetProtusions(int new_amount){ protusions_ = new_amount; }
  int GetProtusions(){ return protusions_; }
  // Return the closest
  // This should probably be integrated in a biology module - this is behaviour, not model
  // Avoid looping through all objects as an object's method
  // Unrealistic biological capacity of a neuriteelement
  SimObject* NeuriteElementInRange(int range){
    auto* sim = Simulation::GetActive();
    auto* rm = sim->GetResourceManager();
    SimObject* valid_neurite = nullptr;
    double distance_from_valid_neurite = range;
    rm->ApplyOnAllElements([&](SimObject* simulationObject){
      // Check if element is a neurite
      if(auto* neurite_2 = dynamic_cast<MyNeurite*>(simulationObject)){
        // Check if it can be created with this element
        if(GetMySoma() != neurite_2->GetMySoma()){
          // They are not of the same type: one is an axon and one is a dendrite
          if(IsAxon() != neurite_2->IsAxon()){
            // Calculate if they are close enough
            auto position = GetPosition();
            auto position_2 = neurite_2->GetPosition();
            double diff_squared_x = pow((position_2[0]-position[0]), 2);
            double diff_squared_y = pow((position_2[1]-position[1]), 2);
            double diff_squared_z = pow((position_2[2]-position[2]), 2);
            double distance = sqrt((diff_squared_x+diff_squared_y+diff_squared_z));
            if(distance <= range && distance <= distance_from_valid_neurite){
              valid_neurite = simulationObject;
            }
          }
        }
      }
    });
    return valid_neurite; // Cannot create synapse
  }

  // For the event caller. How would it work? I don't know
  // NeuriteProtusion* ExtendNewProtusion(NeuriteProtusion* prototype);

  // NeuriteProtusion* ExtendNewProtusion(NeuriteProtusion* protusion, double length,
  //                                     double diameter, double length_position,
  //                                     double theta){
    // auto* neurite_element = static_cast<NeuriteElement*>(this)->GetSoPtr<NeuriteElement>();
    // //static_cast<MySoma*>(other)->GetSoPtr<MySoma>();
    // return protusion(neurite_element, length, diameter, length_position, theta);
  // }
  

 private:
  bool can_branch_ = false;
  SoPointer<MySoma> its_soma_;
  double colour_ = 5.0;
  int protusions_ = 0;
};


}  // namespace neuroscience
}  // namespace experimental
}  // namespace bdm

#endif  // MY_NEURITE_H_
