// -----------------------------------------------------------------------------
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef MY_SOMA_H_
#define MY_SOMA_H_

#include "core/sim_object/sim_object.h"
#include "neuroscience/neuron_soma.h"

namespace bdm {

// Define my custom soma MySoma, which extends SomaElement
class MySoma : public experimental::neuroscience::NeuronSoma {
  BDM_SIM_OBJECT_HEADER(MySoma, experimental::neuroscience::NeuronSoma, 1,
                        swc_label_);

 public:
  MySoma() : Base() {}

  virtual ~MySoma() {}

  MySoma(const Double3& position) : Base(position) {}

  /// Default event constructor
  MySoma(const Event& event, SimObject* other, uint64_t new_oid = 0)
      : Base(event, other, new_oid) {}

  void SetLabel(int l) { swc_label_ = l; }
  int GetLabel() const { return swc_label_; }
  void IncreaseLabel() { swc_label_ += 1; }

 private:
  int swc_label_ = 0;
};

}  // namespace bdm

#endif  // MY_SOMA_H_
