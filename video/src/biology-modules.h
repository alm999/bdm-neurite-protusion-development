#ifndef BIOLOGY_MODULES_H
#define BIOLOGY_MODULES_H

#include "biodynamo.h"
#include "my-neurite.h"
#include "synapse.h"

namespace bdm {
namespace experimental {
namespace neuroscience {

struct NeuriteGrowth : public BaseBiologyModule {
    NeuriteGrowth() : BaseBiologyModule(gAllEventIds) {}

    /// Default event constructor
    NeuriteGrowth(const Event& event, BaseBiologyModule* other, 
        uint64_t new_oid = 0): BaseBiologyModule(event, other, new_oid) {}

    /// Create a new instance of this object using the default constructor.
    BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                    uint64_t new_oid = 0) const override {
        return new NeuriteGrowth(event, other, new_oid);
    }

    /// Create a copy of this biology module.
    BaseBiologyModule* GetCopy() const override {
        return new NeuriteGrowth(*this);
    }

    void Run(SimObject* so) override{
        auto* neurite = bdm_static_cast<MyNeurite*>(so);
        Double3 direction = neurite->GetSpringAxis();
        neurite->ElongateTerminalEnd(100, direction);
        // auto* sim = Simulation::GetActive();
        // auto* rm = sim->GetResourceManager();
        // std::cout << rm->GetNumSimObjects() << std::endl;
        // Get the closest neurite it can synapse with
        // if(auto* neurite_2 = neurite->NeuriteElementInRange(1.0);){
        //     neurite->CreateProtusion(neurite_2);
        // }
    }
};

struct PositionUpdate : public BaseBiologyModule {
    PositionUpdate() : BaseBiologyModule(gAllEventIds) {}

    /// Default event constructor
    PositionUpdate(const Event& event, BaseBiologyModule* other, 
        uint64_t new_oid = 0): BaseBiologyModule(event, other, new_oid) {}

    /// Create a new instance of this object using the default constructor.
    BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                    uint64_t new_oid = 0) const override {
        return new PositionUpdate(event, other, new_oid);
    }

    /// Create a copy of this biology module.
    BaseBiologyModule* GetCopy() const override {
        return new PositionUpdate(*this);
    }

    void Run(SimObject* so) override{
      if(auto* bouton = bdm_static_cast<NeuronBouton*>(so)){
        bouton->SetPosition(bouton->GetMother()->GetPosition());
      }
    }
};

struct ProtusionGrowth : public BaseBiologyModule {
    ProtusionGrowth() : BaseBiologyModule(gAllEventIds) {}

    /// Default event constructor
    ProtusionGrowth(const Event& event, BaseBiologyModule* other, 
        uint64_t new_oid = 0): BaseBiologyModule(event, other, new_oid) {}

    BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                    uint64_t new_oid = 0) const override {
        return new ProtusionGrowth(event, other, new_oid);
    }

    /// Create a copy of this biology module.
    BaseBiologyModule* GetCopy() const override {
        return new ProtusionGrowth(*this);
    }

    void Run(SimObject* so) override{
        if(auto* neurite_cast = bdm_static_cast<MyNeurite*>(so)){
            auto neurite = neurite_cast->GetSoPtr<MyNeurite>();
            auto* sim = Simulation::GetActive();
            auto* rm_ = sim->GetResourceManager();
            auto* random = sim->GetRandom();
            if(random->Uniform(0, 1) > 0.99){
                if(neurite->IsAxon()){
                    if(neurite->GetProtusions() == 0 && random->Uniform(0, 1) > 0.90){
                        double bouton_diameter = neurite->GetDiameter() * 1.35;
                        if(bouton_diameter > (neurite->GetActualLength()*0.8)){
                            bouton_diameter = neurite->GetActualLength()*0.8;
                        }
                        auto* bouton = new NeuronBouton(neurite, bouton_diameter);
                        bouton->AddBiologyModule(new PositionUpdate());
                        rm_->push_back(bouton);
                        neurite->AddProtusion();
                    }
                }else{
                    double theta = random->Uniform(0, 3.1415);
                    double diameter = neurite->GetDiameter() / 4;
                    auto* spine = new NeuronSpine(neurite, 0.2, diameter, theta);
                    rm_->push_back(spine);
                    neurite->AddProtusion();
                }
            }
        }
    }
};

struct RandomizeColour : public BaseBiologyModule {
    RandomizeColour() : BaseBiologyModule(gAllEventIds) {}

    /// Default event constructor
    RandomizeColour(const Event& event, BaseBiologyModule* other, 
        uint64_t new_oid = 0): BaseBiologyModule(event, other, new_oid) {}

    /// Create a new instance of this object using the default constructor.
    BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                    uint64_t new_oid = 0) const override {
        return new RandomizeColour(event, other, new_oid);
    }

    /// Create a copy of this biology module.
    BaseBiologyModule* GetCopy() const override {
        return new RandomizeColour(*this);
    }

    void Run(SimObject* so) override{
      if(auto* neurite = bdm_static_cast<MyNeurite*>(so)){
        double colour = neurite->GetColour();
        auto* sim = Simulation::GetActive();
        auto* random = sim->GetRandom();
        colour = colour + random->Uniform(-1, 1);;
        if(colour < 10 && colour > 0){
          neurite->SetColour(colour);
        }
      }
    }
};

enum Substances { kSubstanceApical, kSubstanceBasal };

struct ApicalElongation_BM : public BaseBiologyModule {
  ApicalElongation_BM() : BaseBiologyModule(gAllEventIds) {}

  /// Default event constructor
  ApicalElongation_BM(const Event& event, BaseBiologyModule* other,
                      uint64_t new_oid = 0)
      : BaseBiologyModule(event, other, new_oid) {}

  /// Create a new instance of this object using the default constructor.
  BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                 uint64_t new_oid = 0) const override {
    return new ApicalElongation_BM(event, other, new_oid);
  }

  /// Create a copy of this biology module.
  BaseBiologyModule* GetCopy() const override {
    return new ApicalElongation_BM(*this);
  }

  // TODO: don't copy BM when split (elongate)

  void Run(SimObject* so) override {
    auto* sim = Simulation::GetActive();
    auto* random = sim->GetRandom();
    auto* rm = sim->GetResourceManager();

    if (!init_) {
      dg_guide_ = rm->GetDiffusionGrid(kSubstanceApical);
      init_ = true;
    }

    auto* dendrite = bdm_static_cast<MyNeurite*>(so);
    if (dendrite->GetDiameter() > 0.5) {
      Double3 gradient;
      dg_guide_->GetGradient(dendrite->GetPosition(), &gradient);
      // double concentration = 0;

      double gradient_weight = 0.06;
      double randomness_weight = 0.3;
      double old_direction_weight = 4;

      Double3 random_axis = {random->Uniform(-1, 1), random->Uniform(-1, 1),
                             random->Uniform(-1, 1)};
      auto old_direction = dendrite->GetSpringAxis() * old_direction_weight;
      auto grad_direction = gradient * gradient_weight;
      auto random_direction = random_axis * randomness_weight;

      Double3 new_step_direction =
          old_direction + random_direction + grad_direction;

      dendrite->ElongateTerminalEnd(100, new_step_direction);
      dendrite->SetDiameter(dendrite->GetDiameter() - 0.00071);

      if (dendrite->GetCanBranch() && dendrite->IsTerminal() &&
          dendrite->GetDiameter() > 0.55 && random->Uniform() < 0.030) {
        auto rand_noise = random->template UniformArray<3>(-0.1, 0.1);
        Double3 branch_direction =
            Math::Perp3(dendrite->GetUnitaryAxisDirectionVector() + rand_noise,
                        random->Uniform(0, 1)) +
            dendrite->GetSpringAxis();
        auto* dendrite_2 =
            bdm_static_cast<MyNeurite*>(dendrite->Branch(branch_direction));
        dendrite_2->SetCanBranch(false);
        dendrite_2->SetDiameter(0.65);
      }

    }  // end if diameter
  }    // end run

 private:
  bool init_ = false;
  DiffusionGrid* dg_guide_ = nullptr;
  BDM_CLASS_DEF_OVERRIDE(ApicalElongation_BM, 1);
};  // end ApicalElongation_BM

struct BasalElongation_BM : public BaseBiologyModule {
  BasalElongation_BM() : BaseBiologyModule(gAllEventIds) {}

  /// Default event constructor
  BasalElongation_BM(const Event& event, BaseBiologyModule* other,
                     uint64_t new_oid = 0)
      : BaseBiologyModule(event, other, new_oid) {}

  /// Create a new instance of this object using the default constructor.
  BaseBiologyModule* GetInstance(const Event& event, BaseBiologyModule* other,
                                 uint64_t new_oid = 0) const override {
    return new BasalElongation_BM(event, other, new_oid);
  }

  /// Create a copy of this biology module.
  BaseBiologyModule* GetCopy() const override {
    return new BasalElongation_BM(*this);
  }

  // TODO: don't copy BM when split (elongate)

  void Run(SimObject* so) override {
    auto* sim = Simulation::GetActive();
    auto* random = sim->GetRandom();
    auto* rm = sim->GetResourceManager();

    if (!init_) {
      dg_guide_ = rm->GetDiffusionGrid(kSubstanceBasal);
      init_ = true;
    }

    auto* dendrite = bdm_static_cast<MyNeurite*>(so);
    if (dendrite->IsTerminal() && dendrite->GetDiameter() > 0.7) {
      Double3 gradient;
      dg_guide_->GetGradient(dendrite->GetPosition(), &gradient);
      // double concentration = 0;

      double gradient_weight = 0.03;
      double randomness_weight = 0.4;
      double old_direction_weight = 6;

      Double3 random_axis = {random->Uniform(-1, 1), random->Uniform(-1, 1),
                             random->Uniform(-1, 1)};

      auto old_direction = dendrite->GetSpringAxis() * old_direction_weight;
      auto grad_direction = gradient * gradient_weight;
      auto random_direction = random_axis * randomness_weight;

      Double3 new_step_direction =
          old_direction + random_direction + grad_direction;

    //   #pragma omp critical
    //   std::cout << old_direction << " " << new_step_direction << std::endl;

      dendrite->ElongateTerminalEnd(50, new_step_direction);
      dendrite->SetDiameter(dendrite->GetDiameter() - 0.00085);

      if (random->Uniform() < 0.006) {
        dendrite->Bifurcate();
      }

    }  // end if diameter

  }  // end run

 private:
  bool init_ = false;
  DiffusionGrid* dg_guide_ = nullptr;
  BDM_CLASS_DEF_OVERRIDE(BasalElongation_BM, 1);
};



} // namespace neuroscience
} // namespace experimental
} // namespace bdm

#endif