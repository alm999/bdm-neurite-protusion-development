#ifndef SYNAPSE_H
#define SYNAPSE_H

#include "core/shape.h"
#include "core/sim_object/sim_object.h"

namespace bdm{
namespace experimental{
namespace neuroscience{

// With a cylindrical geometry, 

// actual length - NA
// Mass_location is the distal end of the cylinder - NA
// Spring_axis - how far away in the other direction is the proximal point - NA (relative to length)
// Position is the middle point of the cylinder - A
// Volume - A

// Proximal end = mass_location - spring_axis

// Unsure
// x_axis  local coordinate axis?
// y_axis
// z_axis

// growth similar to neurite extensions
// growth direction
// perpendicular to the NE
// obtain new position
// Need to simplify from phi, only one coordinate axis needed for positioning on the surface of the NE. Another one for the length tho now
// limited by always leaving the centre of the NE
// Phi seems to work on the length axis (leaving from the centre of the NE issue (remotely) addressed?)

// Two angles for a 3D system

// Abstract class as template for protusions of type NeuronSpine and NeuronBouton
class NeuriteProtusion : public SimObject {
    public:
        NeuriteProtusion(){}

        NeuriteProtusion(SoPointer<MyNeurite> mother, double diameter){
            // set attributes of new neurite protusion
            SetMother(mother);
            SetDiameter(diameter);
        }

        // Event constructor. Construct from the ExtendNewProtusion method
        // from the my-neurite class?
        NeuriteProtusion(const Event& event, SimObject* mother, uint64_t new_oid = 0) 
            : SimObject(event, mother, new_oid) {}

        // Event handler
        void EventHandler(const Event& event, SimObject* other1,
                          SimObject* other2 = nullptr) override{
            SimObject::EventHandler(event, other1, other2);
        }
        Double3 CalculateDisplacement(double squared_radius, double dt) override{
            Double3 zero{0, 0, 0};
            return zero;
        }
        void ApplyDisplacement(const Double3& displacement) override{}
        double GetDiameter() const override { return diameter_; }
        const Double3& GetPosition() const override { return position_; }
        SoPointer<MyNeurite> GetMother(){ return mother_; } 
        void SetDiameter(double diameter) override {
            if (diameter > diameter_) {
                SetRunDisplacementForAllNextTs();
            }
            diameter_ = diameter;
        }
        void SetMother(SoPointer<MyNeurite> neurite_element){
            mother_ = neurite_element;
        }
        // Standard 3D space positioning
        void SetPosition(const Double3& position) override {
            position_ = position;
            SetRunDisplacementForAllNextTs();
        }
    
    protected:
        double diameter_;
        double volume_;
        /// position_ is the middle point of cylinder
        Double3 position_ = {{0.0, 0.0, 0.0}};
        SoPointer<MyNeurite> mother_;

};

class NeuronBouton : public NeuriteProtusion{
    BDM_SIM_OBJECT_HEADER(NeuronBouton, NeuriteProtusion, 1, position_, diameter_, volume_);
    // Default constructor to initialize the base class -- need to change this and base (remove constructors)
    NeuronBouton() : Base() {}

    NeuronBouton(SoPointer<MyNeurite> mother, double diameter) : NeuriteProtusion(mother, diameter){
        if(!mother->IsAxon()){
            std::cout << "Bouton created with non-axon mother" << std::endl;
            throw std::bad_exception();
        }
        UpdateVolume();
        SetPosition(mother->GetPosition());
    }

    /// Default event constructor
    NeuronBouton(const Event& event, SimObject* other, uint64_t new_oid = 0)
        : Base(event, other, new_oid) {

    }

    /// Default event handler
    void EventHandler(const Event& event, SimObject* other1,
                        SimObject* other2 = nullptr) override{
        Base::EventHandler(event, other1, other2);
    }

    // Overriden SimObject method - get required data members for visualization
    std::set<std::string> GetRequiredVisDataMembers() const override {
        return {"position_", "diameter_"};
    }
    
    Shape GetShape() const override { return Shape::kSphere; }

    void UpdateVolume() {
        volume_ = Math::kPi / 6 * std::pow(diameter_, 3);
    }
};

class NeuronSpine : public NeuriteProtusion{
    BDM_SIM_OBJECT_HEADER(NeuronSpine, NeuriteProtusion, 1, mass_location_, position_,
                        volume_, diameter_, x_axis_,
                        y_axis_, z_axis_, spring_axis_,
                        actual_length_);

    public:
        NeuronSpine() : Base() {}

        NeuronSpine(SoPointer<MyNeurite> mother, double length, double diameter, double theta)
                    : Base(mother, diameter){
            if(mother->IsAxon()){
                std::cout << "Spine created from an axon mother" << std::endl;
                throw std::bad_exception();
            }
            SetActualLength(length);
            UpdateVolume();

            // Phi rotates the positioning around a sphere located at the central point of the NE,
            // with a diameter of NE
            double phi = 3.1415 * 0.5; // Perpendicular

            double mother_radius = 0.5 * mother->GetDiameter();

            // position in bdm.cells coord
            double x_coord = std::sin(theta) * std::cos(phi);
            double y_coord = std::sin(theta) * std::sin(phi);
            double z_coord = std::cos(theta);
            Double3 axis_direction{
                x_coord * mother->GetXAxis()[0] + y_coord * mother->GetYAxis()[0] +
                    z_coord * mother->GetZAxis()[0],
                x_coord * mother->GetXAxis()[1] + y_coord * mother->GetYAxis()[1] +
                    z_coord * mother->GetZAxis()[1],
                x_coord * mother->GetXAxis()[2] + y_coord * mother->GetYAxis()[2] +
                    z_coord * mother->GetZAxis()[2]};

            // positions & axis in cartesian coord
            auto new_begin_location = mother->GetPosition() + (axis_direction * mother_radius); // On the surface of the mother
            auto new_spring_axis = axis_direction * length; // Length with direction of the protusion
            auto new_mass_location = new_begin_location + new_spring_axis; // Distal end of the protusion

            // set attributes for the positioning of the protusion
            SetSpringAxis(new_spring_axis);
            SetMassLocation(new_mass_location);
            UpdatePosition();
            UpdateLocalCoordinateAxis();
        }

        NeuronSpine(const Event& event, SimObject* mother, uint64_t new_oid = 0) 
            : Base(event, mother, new_oid) {}

        void EventHandler(const Event& event, SimObject* other1,
                            SimObject* other2 = nullptr) override {
            Base::EventHandler(event, other1, other2);
        }

        // Overriden SimObject methods
        std::set<std::string> GetRequiredVisDataMembers() const override {
            return {"mass_location_", "diameter_", "actual_length_", "spring_axis_"};
        }
        
        Shape GetShape() const override { return Shape::kCylinder; }
    
        
        double GetActualLength() const { return actual_length_; }
        const Double3& GetMassLocation() const { return mass_location_; }


        void SetActualLength(double length){
            actual_length_ = length;
            SetRunDisplacementForAllNextTs();
        }

        void SetMassLocation(const Double3 mass_location){
            mass_location_ = mass_location;
            SetRunDisplacementForAllNextTs();
        }
        void SetSpringAxis(const Double3& axis) {
            SetRunDisplacementForAllNextTs();
            spring_axis_ = axis;
        }

        void UpdatePosition(){
            position_ = mass_location_ - (spring_axis_ * 0.5); 
        }
        void UpdateVolume() {
            volume_ = Math::kPi / 4 * diameter_ * diameter_ * actual_length_;
        }
        void UpdateLocalCoordinateAxis() {
            // x (new) = something new
            // z (new) = x (new) cross y(old)
            // y (new) = z(new) cross x(new)
            auto spring_axis_normalized = spring_axis_;
            x_axis_ = spring_axis_normalized.Normalize();
            z_axis_ = Math::CrossProduct(x_axis_, y_axis_);
            double norm_of_z = z_axis_.Norm();
            if (norm_of_z < 1E-10) {  // TODO(neurites) use parameter
                // If new x_axis_ and old y_axis_ are aligned, we cannot use this scheme;
                // we start by re-defining new perp vectors. Ok, we loose the previous
                // info, but this should almost never happen....
                auto* random = Simulation::GetActive()->GetRandom();
                z_axis_ = Math::Perp3(x_axis_, random->Uniform(0, 1));
            } else {
                z_axis_ = z_axis_ * (1 / norm_of_z);
            }
            y_axis_ = Math::CrossProduct(z_axis_, x_axis_);
        }


    private:
        /// First axis of the local coordinate system equal to cylinder axis
        Double3 x_axis_ = {{1.0, 0.0, 0.0}};
        /// Second axis of the local coordinate system.
        Double3 y_axis_ = {{0.0, 1.0, 0.0}};
        /// Third axis of the local coordinate system.
        Double3 z_axis_ = {{0.0, 0.0, 1.0}};
        double actual_length_;
        /// mass_location_ is distal end of the cylinder
        /// NB: Use setter and don't assign values directly
        Double3 mass_location_ = {{0.0, 0.0, 0.0}};
        /// from the attachment point to the mass location
        /// (proximal -> distal).
        /// NB: Use setter and don't assign values directly
        Double3 spring_axis_ = {{0, 0, 0}};
};

class Synapse { // Bond between them

};

} // namespace neuroscience
} // namespace experimental
} // namespace bdm
#endif