// -----------------------------------------------------------------------------
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef VIDEO_H_
#define VIDEO_H_

#include "common.h"

namespace bdm {

/// define substance for neurite attraction
inline void DefineSubstances(const Param* param) {
  ModelInitializer::DefineSubstance(experimental::neuroscience::kSubstanceApical, "substance_apical", 0, 0,
                                    param->max_bound_ / 80);
  ModelInitializer::DefineSubstance(experimental::neuroscience::kSubstanceBasal, "substance_basal", 0, 0,
                                    param->max_bound_ / 80);
  // create substance with gaussian distribution for neurite attraction
  ModelInitializer::InitializeSubstance(
      experimental::neuroscience::kSubstanceApical, "substance_apical",
      GaussianBand(param->max_bound_, 200, Axis::kZAxis));
  ModelInitializer::InitializeSubstance(
      experimental::neuroscience::kSubstanceBasal, "substance_basal",
      GaussianBand(param->min_bound_, 200, Axis::kZAxis));
}

inline int Simulate(int argc, const char** argv) {
  auto set_param = [&](Param* param) {
    param->bound_space_ = true;
    param->min_bound_ = 0;
    param->max_bound_ = 450;
    param->GetModuleParam<experimental::neuroscience::Param>()
        ->neurite_max_length_ = 2.0;
    param->export_visualization_ = true;
    param->visualization_export_interval_ = 1;
    param->detect_static_sim_objects_ = true;
    param->cache_neighbors_ = true;
    // param->GetModuleParam<experimental::neuroscience::Param>()->neurite_default_actual_length_ = 3.0;
  };
  experimental::neuroscience::InitModule();
  CommandLineOptions clo(argc, argv);
  Simulation simulation(&clo, set_param);

  // auto* rm = simulation.GetResourceManager();
  auto* param = simulation.GetParam();

  experimental::neuroscience::NeuronBuilder builder;
  // builder({0, 0, 0});
  builder({50, 50, 50});

  DefineSubstances(param);
  
  // Run simulation for one timestep
  simulation.GetScheduler()->Simulate(100);

  std::cout << "Simulation completed successfully!" << std::endl;
  return 0;
}


}  // namespace bdm

#endif  // VIDEO_H_
