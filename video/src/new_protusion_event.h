#ifndef EVENT_NEW_PROTUSION_EVENT_H_
#define EVENT_NEW_PROTUSION_EVENT_H_

#include "core/event/event.h"

namespace bdm{
namespace experimental{
namespace neuroscience{

struct NewProtusionEvent : public Event{
    static const EventId kEventId;

  NewProtusionEvent(double diameter, double length)
      : diameter_(diameter) {}

  virtual ~NewProtusionEvent() {}

  EventId GetId() const override { return kEventId; }

  /// diameter the diameter of the protusion
  double diameter_;
  double length;
};

} // namespace neuroscience
} // namespace experimental
} // namespace bdm

#endif // EVENT_NEW_PROTUSION_EVENT_H_