lines_in_file_divided_by_100 = 3673

line_count = 0
step = 0

with open("test/output.txt", "r") as f, open("divided/s.txt", "w") as sw, open("divided/l.txt", "w") as lw, open("divided/p.txt", "w") as pw, open("divided/d.txt", "w") as dw:
    for line in f.readlines():
        line_count += 1
        if line_count % 100 == 0:
            print("\r", end='')
            print(line_count, end='')
            for i in range(0, int(line_count/lines_in_file_divided_by_100)):
                print(u'\u2588', end='')
            for i in range(int(line_count/lines_in_file_divided_by_100), 100):
                print(' ', end='')
            print("| ", end='')
            print("%.2f" % float(int(line_count)/int(lines_in_file_divided_by_100)), end='')
            print("% ", end='')


        if "STEP ########" in line: # UPDATE EVERYTHING
            sw.writelines(line)
            lw.writelines(line)
            pw.writelines(line)
            dw.writelines(line)
            step = step + 1
        elif "S" in line:
            sw.writelines(line)
        elif "D" in line:
            dw.writelines(line)
        elif "L" in line:
            lw.writelines(line)
        elif "P" in line:
            pw.writelines(line)
        else:
            print("Found weird output: ")
            print(line)
