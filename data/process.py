# START WITHOUT A STEP#### BUT DIRECTLY INTO DATA

# size of neurites (4) vs time
# S;neuriteid;soptr;size
# MANY PER STEP -> Get largest of each neurite ID
# LENGTH OF EACH NEURITE PER STEP

# length of maximum neurite vs time
# L;length

# D;

# P;


# S
s_graph = [["Neurite 1 size", "Neurite 2 size", "Neurite 3 size", "Neurite 4 size"]]

# D
average_diameter_graph = [["N1 Diameter", "N2 Diameter", "N3 Diameter", "N4 Diameter"]]
diameter_apical_graph = [["Time", "Diameter", "Position in queue"]]
diameter_basal_graph = [["Time", "Diameter", "Position in queue"]]

# L for longest
l_graph = ["Length/size of total neurite"]

# P
terminal_position_data = [["Time", "Apical(0)/Basal(1, 2, 3)", "Terminal ID", "X", "Y", "Z"]] # This data will be saved somewhere and screenshotted
# Would be a 4D graph for each neurite, but if positions are switched to data of length from the soma then might be turned into a branched graph. In R
number_of_terminals_graph = ["Number of terminals"]

step = 0
p_count = 0
avg_diameter_count = [[0, 0, 0, 0], [0, 0, 0, 0]]
sizes = [0.0, 0.0, 0.0, 0.0]

# fake = 0
line_count = 0
lines_in_file_divided_by_100 = 3673

with open("test/output.txt", "r") as f:
    for line_string in f.readlines():
        line = line_string.strip().split(";")
        #print(line)
        line_count += 1
        if line_count % 100 == 0:
            print("\r", end='')
            print(line_count, end='')
            for i in range(0, int(line_count/lines_in_file_divided_by_100)):
                print(u'\u2588', end='')
            for i in range(int(line_count/lines_in_file_divided_by_100), 100):
                print(' ', end='')
            print("| ", end='')
            print("%.2f" % float(int(line_count)/int(lines_in_file_divided_by_100)), end='')
            print("% ", end='')


        if line[0] == "STEP ########": # UPDATE EVERYTHING
            if step == 0:
                step += 1
                continue
            number_of_terminals_graph.append(p_count)
            p_count = 0

            if avg_diameter_count[1][0] == 0:
                avg_diameter_count[0][0] = average_diameter_graph[-1][0]
                avg_diameter_count[1][0] = 1
            if avg_diameter_count[1][1] == 0:
                avg_diameter_count[1][1] = 1
                avg_diameter_count[0][1] = average_diameter_graph[-1][1]
            if avg_diameter_count[1][2] == 0:
                avg_diameter_count[1][2] = 1
                avg_diameter_count[0][2] = average_diameter_graph[-1][2]
            if avg_diameter_count[1][3] == 0:
                avg_diameter_count[1][3] = 1
                avg_diameter_count[0][3] = average_diameter_graph[-1][3]
            
            try:
                average_diameter_graph.append([avg_diameter_count[0][0]/avg_diameter_count[1][0], avg_diameter_count[0][1]/avg_diameter_count[1][1], avg_diameter_count[0][2]/avg_diameter_count[1][2], avg_diameter_count[0][3]/avg_diameter_count[1][3]])
                avg_diameter_count = [[0, 0, 0, 0], [0, 0, 0, 0]]
            except:
                print(step, " - ", line_count)
                print(average_diameter_graph[-1])
                print(avg_diameter_count)
                raise

            if len(s_graph) > 1:
                if sizes[0] < s_graph[-1][0]:
                    sizes[0] = s_graph[-1][0]
                if sizes[1] < s_graph[-1][1]:
                    sizes[1] = s_graph[-1][1]
                if sizes[2] < s_graph[-1][2]:
                    sizes[2] = s_graph[-1][2]
                if sizes[3] < s_graph[-1][3]:
                    sizes[3] = s_graph[-1][3]
            s_graph.append(sizes);
            sizes = [0.0, 0.0, 0.0, 0.0]

            step = step + 1
        elif line[0] == "S":
            if sizes[int(line[1])] < float(line[3]):
                sizes[int(line[1])] = float(line[3])
        elif line[0] == "D":
            # fake \/
            # line.append(line[2])
            # line[2] = line[1]
            # line[1] = str(fake)
            # fake /\
            avg_diameter_count[0][int(line[1])] = avg_diameter_count[0][int(line[1])] + float(line[3])
            avg_diameter_count[1][int(line[1])] = avg_diameter_count[1][int(line[1])] + 1

            # calculate position
            uid = line[2].split(" ")[2].split("-")[0]
            if line[1] == "0":
                diameter_apical_graph.append([step, line[3], uid])
            elif line[1] == "1":
                diameter_basal_graph.append([step, line[3], uid])

            # fake \/
            # if fake == 3:
            #     fake = 0
            # else:
            #     fake = fake + 1
            # fake /\

        elif line[0] == "L":
            try:
                if float(line[1]) > float(l_graph[-1]):
                    l_graph.append(line[1])
                else:
                    l_graph.append(l_graph[-1])
            except ValueError: # step 1
                l_graph.append(line[1])
        elif line[0] == "P":
            # Count terminals
            p_count = p_count + 1
            # Update terminal position data
            position = line[2].replace(",", "").split(" ")
            terminal_position_data.append([step, line[1], line[2], position[0], position[1], position[2]])
        else:
            print("Found weird output: ")
            print(line)


with open("formatted/s_graph.txt", "w") as f:
    for i in range(0, len(s_graph)):
        f.write(str(str(s_graph[i][0]) + ";" + str(s_graph[i][1]) + ";" + str(s_graph[i][2]) + ";" + str(s_graph[i][3]) + "\n").replace(".", ","))

with open("formatted/average_diameter_graph.txt", "w") as f:
    for i in range(0, len(average_diameter_graph)):
        f.write(str(str(average_diameter_graph[i][0]) + ";" + str(average_diameter_graph[i][1]) + ";" + str(average_diameter_graph[i][2]) + ";" + str(average_diameter_graph[i][3]) + "\n").replace(".", ","))

with open("formatted/diameter_apical_graph.txt", "w") as f:
    for i in range(0, len(diameter_apical_graph)):
        f.write(str(str(diameter_apical_graph[i][0]) + ";" + str(diameter_apical_graph[i][1]) + ";" + str(diameter_apical_graph[i][2]) + "\n").replace(".", ","))

with open("formatted/diameter_basal_graph.txt", "w") as f:
    for i in range(0, len(diameter_basal_graph)):
        f.write(str(str(diameter_basal_graph[i][0]) + ";" + str(diameter_basal_graph[i][1]) + ";" + str(diameter_basal_graph[i][2]) + "\n").replace(".", ","))

with open("formatted/l_graph.txt", "w") as f:
    for i in range(0, len(l_graph)):
        f.write(str(str(l_graph[i]) + "\n").replace(".", ","))

with open("formatted/terminal_position_data.txt", "w") as f:
    for i in range(0, len(terminal_position_data)):
        f.write(str(str(terminal_position_data[i][0]) + ";" + str(terminal_position_data[i][1]) + ";" + str(terminal_position_data[i][2]) + ";" + str(terminal_position_data[i][3]) + ";" + str(terminal_position_data[i][4]) + ";" + str(terminal_position_data[i][5]) + "\n").replace(".", ","))

with open("formatted/number_of_terminals_graph.txt", "w") as f:
    for i in range(0, len(number_of_terminals_graph)):
        f.write(str(str(number_of_terminals_graph[i]) + "\n").replace(".", ","))


# for i in range(0, len(diameter_apical_graph[0])):
#     print(diameter_apical_graph[0][i])
