// -----------------------------------------------------------------------------
//
// Copyright (C) The BioDynaMo Project.
// All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// See the LICENSE file distributed with this work for details.
// See the NOTICE file distributed with this work for additional information
// regarding copyright ownership.
//
// -----------------------------------------------------------------------------
#ifndef CELLS_H
#define CELLS_H

#include "biodynamo.h"
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

namespace bdm {

/// This biology module grows the simulation object until the diameter reaches
/// the specified threshold and divides the object afterwards.
struct GrowDivideModified : public BaseBiologyModule {
  BDM_BM_HEADER(GrowDivideModified, BaseBiologyModule, 1);
  GrowDivideModified() : BaseBiologyModule(gAllEventIds) {}
  GrowDivideModified(double threshold, double growth_rate,
             std::initializer_list<EventId> event_list)
      : BaseBiologyModule(event_list),
        threshold_(threshold),
        growth_rate_(growth_rate) {}

  GrowDivideModified(const Event& event, BaseBiologyModule* other, uint64_t new_oid = 0)
      : BaseBiologyModule(event, other, new_oid) {
    if (GrowDivideModified* gdbm = dynamic_cast<GrowDivideModified*>(other)) {
      threshold_ = gdbm->threshold_;
      growth_rate_ = gdbm->growth_rate_;
    } else {
      Log::Fatal("GrowDivideModified::EventConstructor",
                 "other was not of type GrowDivideModified");
    }
  }

  /// Default event handler (exising biology module won't be modified on
  /// any event)
  
  void Run(SimObject* so) override {
    if (Cell* cell = dynamic_cast<Cell*>(so)) {
      // Calculate chance if cell is large enough
      double chance = 1;
      if(cell->GetDiameter() > threshold_){
        chance = threshold_ / (cell->GetDiameter()*1.25);
      }

      #pragma omp critical
      std::cout << setw(7) << cell->GetUid() << setw(15) << cell->GetDiameter() << setw(15) << 1-chance << std::endl;
    //   File output
    //   std::cout << cell->GetDiameter() << ";" << 1-chance << std::endl;
    

      
      // If cell is larger, randomize 
      if ((cell->GetDiameter() > threshold_) && (double((rand()) / double(RAND_MAX)) > chance)) { 
        cell->Divide();
        // cell->ChangeVolume(growth_rate_); // for graph
      }else{
        cell->ChangeVolume(growth_rate_);
      }
    } else {
      Log::Fatal("GrowDivideModified::Run", "SimObject is not a Cell");
    }
  }

 private:
  double threshold_ = 40;
  double growth_rate_ = 300;
};

inline int Simulate(int argc, const char** argv) {
  Simulation simulation(argc, argv);

  // Define cell models
  auto* rm = simulation.GetResourceManager();
  Cell * cell = new Cell(30);
//   cell->SetPosition({{50, 50, 50}});
  cell->AddBiologyModule(new GrowDivideModified(32, 6000, {gAllEventIds}));
  rm->push_back(cell);


  Double3 position = cell->GetPosition() + Double3({{50, 0, 0}});
  cell = new Cell(position);
  cell->SetDiameter(30);
  rm->push_back(cell);

  std::cout << setw(7) << "CELL UID" << setw(15) << "DIAMETER" << setw(15) << "CHANCE" << std::endl << std::endl;
  
  srand (time(NULL));
  // Run simulation
  simulation.GetScheduler()->Simulate(450);

  std::cout << "Simulation completed successfully!" << std::endl;
  return 0;
}

}  // namespace bdm

#endif  // CELLS_H
